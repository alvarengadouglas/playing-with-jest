import NovoLeilao from '@/views/NovoLeilao'
import { mount } from '@vue/test-utils'
import { createLeilao } from '@/http'

jest.mock('@/http')

const $router = {
    push: jest.fn()
}

describe('Um novo lailão deve ser criado', () => {
    test('Dado o formulário preenchido, um leilão deve ser criado', () => {
        createLeilao.mockResolvedValueOnce()

        const wrapper = mount(NovoLeilao, {
            mocks: {
                $router
            }
        })

        const inputProduto = wrapper.find('.produto')
        const inputDescricao = wrapper.find('.descricao')
        const inputValor = wrapper.find('.valor')

        inputProduto.setValue('Livro sobre Jest')
        inputDescricao.setValue('Livro para aprender sobre o framework Jest')
        inputValor.setValue(100)

        wrapper.find('form').trigger('submit')
        expect(createLeilao).toHaveBeenCalled()
    })
})