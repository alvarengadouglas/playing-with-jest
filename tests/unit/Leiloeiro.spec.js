import Leiloeiro from '@/views/Leiloeiro'
import { mount } from '@vue/test-utils'
import { getLeilao, getLances } from '@/http'
import flushPromises from 'flush-promises'

jest.mock('@/http')

const leilao = {
    produto: 'Um livro de vue',
    lanceInicial: 50,
    descricao: 'Um livro super bacana sobre vue'
}

const lances = [
    {
        id: 1,
        valor: 50,
        data: '2022-04-03',
        lance_id: 1       
    },
    {
        id: 2,
        valor: 150,
        data: '2022-02-03',
        lance_id: 2       
    },
    {
        id: 3,
        valor: 250,
        data: '2022-03-03',
        lance_id: 3       
    }
]

describe('Leiloeiro inicia um leilão que não possui lances', () => {
    test('avisa quando não existem lances', async () => {

        getLeilao.mockResolvedValueOnce(leilao)
        getLances.mockResolvedValueOnce([])

        const wrapper = mount(Leiloeiro, {
            propsData: {
                id: 1
            }
        })

        await flushPromises()

        const alerta = wrapper.find('.alert-dark')
        expect(alerta.exists()).toBe(true)
    })
})

describe('Um leiloeiro exibe todos os lances existentes', () => {
    test('Não monstra o aviso de "sem lances"', async () => {
        getLeilao.mockResolvedValueOnce(leilao)
        getLances.mockResolvedValueOnce(lances)

        const wrapper = mount(Leiloeiro, {
            propsData: {
                id: 1
            }
        })

        await flushPromises()

        const alerta = wrapper.find('.alert-dark')
        expect(alerta.exists()).toBe(false)
    })

    test('Possui um lista de lances', async () => {
        getLeilao.mockResolvedValueOnce(leilao)
        getLances.mockResolvedValueOnce(lances)

        const wrapper =  mount(Leiloeiro, {
            propsData: {
                id: 1
            }
        })

        await flushPromises()

        const lista = wrapper.find('.list-inline')
        expect(lista.exists()).toBe(true)

    })
})

describe('Um leiloeiro comunica os valores de menor e maior lance', () => {
    test('Mostra o maior lance daquele leilão', async () => {
        getLeilao.mockResolvedValueOnce(leilao)
        getLances.mockResolvedValueOnce(lances)

        const wrapper = mount(Leiloeiro, {
            propsData: {
                id: 1
            }
        })

        await flushPromises()

        const maiorLance = wrapper.find('.maior-lance').element
        expect(maiorLance.textContent).toBe('Maior lance: R$ 250')
    })

    test('Mostra o menor lance daquele leilão', async () => {
        getLeilao.mockResolvedValueOnce(leilao)
        getLances.mockResolvedValueOnce(lances)

        const wrapper = mount(Leiloeiro, {
            propsData: {
                id: 1
            }
        })

        await flushPromises()

        const menorLance = wrapper.find('.menor-lance').element
        expect(menorLance.textContent).toBe('Menor lance: R$ 50')
    })
})
