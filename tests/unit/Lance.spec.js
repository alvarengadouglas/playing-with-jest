import Lance from '@/components/Lance'
import { mount } from '@vue/test-utils'

test('não aceita lance com valor menor que zero', () => {
    const wrapper = mount(Lance)
    const input = wrapper.find('input')
    input.setValue(-300)
    const lancesEmitidos = wrapper.emitted('novo-lance')
    wrapper.trigger('submit')
    expect(lancesEmitidos).toBeUndefined()
})

test('emite um lance quando valor é maior do que zero', () => {
    const wrapper = mount(Lance)
    const input = wrapper.find('input')
    input.setValue(300)
    wrapper.trigger('submit')
    const lancesEmitidos = wrapper.emitted('novo-lance')
    expect(lancesEmitidos).toHaveLength(1)
})

test('emite o valor esperado de mais um lance válido', () => {
    const wrapper = mount(Lance)
    const input = wrapper.find('input')
    input.setValue(100)
    wrapper.trigger('submit')
    const lancesEmitidos = wrapper.emitted('novo-lance')
    const lance = parseInt(lancesEmitidos[0][0])
    expect(lance).toBe(100)
})

describe('um lance com valor mínimo', () => {
    test('todos dos lances devem possuir um valor maior do que o mínimo informado', () => {
        const wrapper = mount(Lance, {
            propsData: {
                lanceMinimo: 300
            }
        })

        const input = wrapper.find('input')
        input.setValue(400)
        wrapper.trigger('submit')
        const lancesEmitidos = wrapper.emitted('novo-lance')
        expect(lancesEmitidos).toHaveLength(1)

    })

    test('emite o valor esperado de um lance valido', () => {
        const wrapper = mount(Lance, {
            propsData: {
                lanceMinimo: 300
            }
        })

        const input = wrapper.find('input')
        input.setValue(400)
        wrapper.trigger('submit')
        const lancesEmitidos = wrapper.emitted('novo-lance')
        const lance = parseInt(lancesEmitidos[0][0])
        expect(lance).toBe(400)

    })

    test('não são aceitos lances com valores menores do que o mínimo informado', async () => {
        const wrapper = mount(Lance, {
            propsData: {
                lanceMinimo: 300
            }
        })

        const input = wrapper.find('input')
        input.setValue(100)
        wrapper.trigger('submit')
        await wrapper.vm.$nextTick()
        const msgErro = wrapper.find('p.alert').element.textContent
        const msgEsperada = 'O valor mínimo para o lance é de R$ 300'
        expect(msgErro).toBeTruthy()
        expect(msgErro).toContain(msgEsperada)

    })
})