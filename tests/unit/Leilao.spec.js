import Leilao from '@/components/Leilao'
import { mount } from '@vue/test-utils'

const leilao = {
    produto: 'Um livro da casa do código',
    lanceInicial: 49,
    descricao: 'Um maravilhoso livro sobre VUE'
}

describe('Um leilão exibe dados do produto', () => {
    test('exibe os dados do leilão no card', () => {
        const wrapper = mount(Leilao, {
            propsData: {
                leilao
            }
        })

        const header = wrapper.find('.card-header').element
        const expectedHeaderText = `Estamos leiloando um(a): ${ leilao.produto }`        
        expect(header.textContent).toContain(expectedHeaderText)

        const title = wrapper.find('.card-title').element
        const expectedTitleText = `Lance inicial: R$ ${ leilao.lanceInicial }`
        expect(title.textContent).toContain(expectedTitleText)

        const description = wrapper.find('.card-text').element
        const expectedDescriptionText = `${ leilao.descricao }`
        expect(description.textContent).toContain(expectedDescriptionText)

    })
})